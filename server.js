const express = require("express");
const cors = require("cors");
const uuid = require("uuid");
const bodyParser = require("body-parser");
const apiV1 = require('./api/v1');

require("dotenv").config();
const http = require("http");

const { PORT, CURRENT_VERSION } = process.env;

const app = express();
const server = http.createServer(app);

app.use(
    cors({
        origin: "*",
        credentials: true,
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        allowedHeaders:
            "Content-Type,Authorization,X-Requested-With,Accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers",
        exposedHeaders:
            "Content-Type,Authorization,X-Requested-With,Accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers",
        optionsSuccessStatus: 200,
    })
);

app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
    res.status(200).json({ message: "Hello world!" });
});

app.get("/ping", (req, res) => {
    res.status(200).json({ message: "pong" });
});

if (CURRENT_VERSION === "1") {
    app.use(apiV1);
} else {
    return;
}

app.get("*", (req, res) => {
    res.status(404).json({ message: "Not found" });
});

server.listen(PORT, () => {
    console.log("Server started on port", PORT);
});