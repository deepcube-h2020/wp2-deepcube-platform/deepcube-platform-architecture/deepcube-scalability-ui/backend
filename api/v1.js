const express = require('express');
const app = express();
const mysql = require("mysql2");
const uuid = require("uuid");
const bodyParser = require("body-parser");
const { exec } = require("child_process");


require("dotenv").config();

const { DB_HOST, DB_NAME, DB_USER, DB_PASS, PASSWORD , DB_PORT} = process.env;

app.use(bodyParser.urlencoded({ extended: false }));

const db = mysql.createConnection({
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASS,
    database: DB_NAME,
    port: DB_PORT,
});

//	1	id_node Primary	int(11)
// uuid	varchar(255)
// 2	email	varchar(255)
// 3	creation_date	timestamp
// activate_date	
// 4	destroy_date timestamp
// 5	usecase	varchar(255)
// 6	type varchar(100)
// 7 duration int(11)
// 8	status	varchar(100)

db.connect((err) => {
    if (err) {
        console.error("Error connecting to database:", err);
    } else {
        console.log("Connected to database!");
    }
});

const VERSION = "/api/v1";

app.get(`${VERSION}`, (req, res) => {
    res.status(200).json({ message: "Api v1" });
});

app.get(`${VERSION}/type/list`, (req, res) => {
    let types = ["GPU", "CPU"];
    res.status(200).json({ types: types });
});

app.get(`${VERSION}/usecase/list`, (req, res) => {
    let usecases = [
        {
            name: "Vegetation Forecasting",
            code: "UC1",
        },
        {
            name: "Climate induced migration in Africa",
            code: "UC2",
        },
        {
            name: "Fire hazard short-term forecasting in the Mediterranean",
            code: "UC3",
        },
        {
            name: "Global automatic volcanic deformation detection and alerting",
            code: "UC4a",
        },
        {
            name: "Automatic critical infrastructure monitoring",
            code: "UC4b",
        },
        {
            name: "Copernicus services for sustainable and environmentally friendly tourism",
            code: "UC5",
        },
    ];
    res.status(200).json({ usecases: usecases });
});

app.post(`${VERSION}/auth/login`, (req, res) => {
    let password = req.body.password;

    if (!password) {
        res.status(200).json({ error: "Missing parameters" });
        return;
    }

    if (password !== PASSWORD) {
        res.status(200).json({ error: "wrong" });
        return;
    }

    res.status(200).json({ message: "success" });
});

app.post(`${VERSION}/node/add`, (req, res) => {
    let email = req.body.email;
    let usecase = req.body.usecase;
    let type = req.body.type;
    let id_node = uuid.v4();
    let duration = req.body.destroy_date;
    let numberOfNodes = req.body.numberOfNodes;

    if (!email || !usecase || !type || !duration) {
        res.status(200).json({ error: "Missing parameters" });
        return;
    }

    //verifier si email est valide
    if (!email.includes("@") || !email.includes(".")) {
        res.status(200).json({ error: "Email not valid" });
        return;
    }

    //verifier si usecase c'est UC1, UC2, UC3, UC4a, UC4b ou UC5
    if (usecase !== "UC1" && usecase !== "UC2" && usecase !== "UC3" && usecase !== "UC4a" && usecase !== "UC4b" && usecase !== "UC5") {
        res.status(200).json({ error: "Usecase not found" });
        return;
    }

    //verifier si le type c'est GPU ou CPU
    if (type !== "GPU" && type !== "CPU") {
        res.status(200).json({ error: "Type not found" });
        return;
    }

    //verifier si la date c'est bien entre 1 et 7 jours
    if (duration < 1 || duration > 7) {
        res.status(200).json({ error: "Duration date is conform" });
        return;
    }

    if (numberOfNodes < 1 || numberOfNodes > 5) {
        res.status(200).json({ error: "Number of nodes is not conform" });
        return;
    }

    for (let i = 0; i < numberOfNodes; i++) {
        id_node = uuid.v4();
        let sql = `INSERT INTO node (uuid, email, usecase, type, duration, status) VALUES ('${id_node}', '${email}', '${usecase}', '${type}', '${duration}', 'pendingprovisioning')`;
        db.query(sql, (err, result) => {
            if (err) {
                if (i === numberOfNodes - 1) {
                    res.status(200).json({ error: "Error adding node" });
                }
            } else {
                if (i === numberOfNodes - 1) {
                    res.status(200).json({ message: "Node added successfully" });
                }
            }
        });
    }
});

app.post(`${VERSION}/node/provisioning`, (req, res) => {
    let uuid = req.body.uuid;

    if (!uuid) {
        res.status(200).json({ error: "Missing parameters" });
        return;
    }

    let sql = `SELECT * FROM node WHERE uuid = '${uuid}' AND status = 'pendingprovisioning'`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error provisioning node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ error: "Node not found or already activated" });
                return;
            } else {
                sql = `UPDATE node SET status = 'provisioning' WHERE uuid = '${uuid}'`;
                db.query(sql, (err, result) => {
                    if (err) {
                        res.status(200).json({ error: "Error provisioning node" });
                    } else {
                        res.status(200).json({ message: "Node provisioning successfully" });
                    }
                });
            }
        }
    });
});


app.post(`${VERSION}/node/activate`, (req, res) => {
    let uuid = req.body.uuid;

    if (!uuid) {
        res.status(200).json({ error: "Missing parameters" });
        return;
    }

    let sql = `SELECT * FROM node WHERE uuid = '${uuid}' AND status = 'provisioning'`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error activating node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ error: "Node not found or already activated" });
                return;
            } else {
                sql = `UPDATE node SET status = 'active', activate_date = NOW() WHERE uuid = '${uuid}'`;
                db.query(sql, (err, result) => {
                    if (err) {
                        res.status(200).json({ error: "Error activating node" });
                    } else {
                        res.status(200).json({ message: "Node activated successfully" });
                    }
                });
            }
        }
    });
});


app.put(`${VERSION}/node/update`, (req, res) => {
    let uuid = req.body.uuid;
    let duration = Number(req.body.duration);  // convert to number

    if (!uuid || !duration) {
        res.status(200).json({ error: "Missing parameters" });
        return;
    }

    // verifier si la date c'est bien entre 1 et 7 jours
    if (duration < 1 || duration > 7) {
        res.status(200).json({ error: "Duration date is conform" });
        return;
    }

    let sql = `SELECT * FROM node WHERE uuid = '${uuid}' AND status = 'active'`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error updating node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ error: "Node not found or already destroyed" });
                return;
            } else {
                let new_duration = result[0].duration + duration;  // now duration is a number
                sql = `UPDATE node SET duration = '${new_duration}' WHERE uuid = '${uuid}'`;
                db.query(sql, (err, result) => {
                    if (err) {
                        res.status(200).json({ error: "Error updating node" });
                    } else {
                        res.status(200).json({ message: "Node updated successfully" });
                    }
                });
            }
        }
    });
});

app.post(`${VERSION}/node/pendingdestroy`, (req, res) => {
    let uuid = req.body.uuid;
    if (!uuid) {
        res.status(200).json({ error: "Missing parameters" });
        return;
    }

    let sql = `SELECT * FROM node WHERE uuid = '${uuid}' AND status = 'active'`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error pending delete node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ error: "Node not found or already destroyed" });
                return;
            } else {
                sql = `UPDATE node SET status = 'pendingdestroy' WHERE uuid = '${uuid}'`;
                db.query(sql, (err, result) => {
                    if (err) {
                        res.status(200).json({ error: "Error pending delete node" });
                    } else {
                        res.status(200).json({ message: "Node pending delete successfully" });
                        // exec(`curl -X DELETE -H "Content-Type: application/x-www-form-urlencoded" -d 'uuid=${uuid}' http://localhost:3001/api/v1/node/destroy`, (err, stdout, stderr) => {
                        //     if (err) {
                        //         console.error("Error destroying node:", err);
                        //     } else {
                        //         console.log("Node destroyed successfully");
                        //     }
                        // });
                    }
                });
            }
        }
    });
});

app.delete(`${VERSION}/node/destroy`, (req, res) => {
    let uuid = req.body.uuid;
    //METTRE LA DATE SOUS LE FORMAT YYYY-MM-DD HH:MM:SS heure de paris
    let destroy_date = new Date();

    let offset = -1 * (new Date().getTimezoneOffset() / 60); // get local timezone offset
    let parisOffset = 2; // Paris timezone is UTC +2
    destroy_date.setHours(destroy_date.getHours() + parisOffset - offset);

    let year = destroy_date.getFullYear();
    let month = ("0" + (destroy_date.getMonth() + 1)).slice(-2); // Months are 0 based
    let day = ("0" + destroy_date.getDate()).slice(-2);
    let hours = ("0" + destroy_date.getHours()).slice(-2);
    let minutes = ("0" + destroy_date.getMinutes()).slice(-2);
    let seconds = ("0" + destroy_date.getSeconds()).slice(-2);

    destroy_date = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;

    if (!uuid) {
        res.status(200).json({ error: "Missing parameters" });
        return;
    }

    let sql = `SELECT * FROM node WHERE uuid = '${uuid}'`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error destroying node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ error: "Node not found" });
                return;
            } else {
                sql = `SELECT * FROM node WHERE uuid = '${uuid}' AND status = 'destroyed'`;
                db.query(sql, (err, result) => {
                    if (err) {
                        res.status(200).json({ error: "Error destroying node" });
                    } else {
                        if (result.length !== 0) {
                            res.status(200).json({ error: "Node already destroyed" });
                            return;
                        } else {
                            sql = `UPDATE node SET status = 'destroyed', destroy_date = '${destroy_date}' WHERE uuid = '${uuid}'`;
                            db.query(sql, (err, result) => {
                                if (err) {
                                    res.status(200).json({ error: "Error destroying node" });
                                } else {
                                    res.status(200).json({ message: "Node destroyed successfully" });
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});

app.get(`${VERSION}/node/cron`, (req, res) => {
    let sql = `SELECT * FROM node WHERE status = 'active'`;
    db.query(sql, (err, result) => {
        if (err) {
            console.error("Error cron node:", err);
            res.status(200).json({ error: "Error cron node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ message: "No node to destroy" });
                return;
            } else {
                for (let i = 0; i < result.length; i++) {
                    //faire en sorte que la date soit au format YYYY-MM-DD HH:MM:SS
                    let date_node = new Date(result[i].creation_date);
                    let offset = -1 * (new Date().getTimezoneOffset() / 60); // get local timezone offset
                    let parisOffset = 2; // Paris timezone is UTC +2
                    date_node.setHours(date_node.getHours() + parisOffset - offset);

                    let year = date_node.getFullYear();
                    let month = ("0" + (date_node.getMonth() + 1)).slice(-2); // Months are 0 based
                    let day = ("0" + date_node.getDate()).slice(-2);
                    let hours = ("0" + date_node.getHours()).slice(-2);
                    let minutes = ("0" + date_node.getMinutes()).slice(-2);
                    let seconds = ("0" + date_node.getSeconds()).slice(-2);

                    date_node = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;

                    let date_now = new Date();
                    offset = -1 * (new Date().getTimezoneOffset() / 60); // get local timezone offset
                    parisOffset = 2; // Paris timezone is UTC +2
                    date_now.setHours(date_now.getHours() + parisOffset - offset);

                    year = date_now.getFullYear();
                    month = ("0" + (date_now.getMonth() + 1)).slice(-2); // Months are 0 based
                    day = ("0" + date_now.getDate()).slice(-2);
                    hours = ("0" + date_now.getHours()).slice(-2);
                    minutes = ("0" + date_now.getMinutes()).slice(-2);
                    seconds = ("0" + date_now.getSeconds()).slice(-2);

                    date_now = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;

                    let date_node_ms = new Date(date_node).getTime();
                    let date_now_ms = new Date(date_now).getTime();

                    let diff = date_now_ms - date_node_ms;
                    let diff_days = Math.floor(diff / (1000 * 60 * 60 * 24));

                    if (diff_days >= result[i].duration) {
                        sql = `UPDATE node SET status = 'pendingdestroy' WHERE uuid = '${result[i].uuid}'`;
                        db.query(sql, (err, result) => {
                            if (err) {
                                res.status(200).json({ error: "Error cron node" });
                            } else {
                                res.status(200).json({ message: "Node pending delete successfully" });
                                exec(`curl -X DELETE -H "Content-Type: application/x-www-form-urlencoded" -d 'uuid=${result[i].uuid}' http://localhost:3001/api/v1/node/destroy`, (err, stdout, stderr) => {
                                    if (err) {
                                        console.error("Error destroying node:", err);
                                    } else {
                                        console.log("Node destroyed successfully");
                                    }
                                });
                            }
                        });
                    } else {
                        res.status(200).json({ message: "No node to destroy" });
                    }
                }
            }
        }
    });
});

app.get(`${VERSION}/node/list`, (req, res) => {
    let sql = `SELECT * FROM node`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error listing node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ message: "No node found" });
                return;
            } else {
                res.status(200).json({ nodes: result });
            }
        }
    });
});

app.get(`${VERSION}/node/list/pendingprovisioning`, (req, res) => {
    let sql = `SELECT * FROM node WHERE status = 'pendingprovisioning'`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error listing node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ message: "No node found" });
                return;
            } else {
                res.status(200).json({ nodes: result });
            }
        }
    });
});

app.get(`${VERSION}/node/list/pendingdestroy`, (req, res) => {
    let sql = `SELECT * FROM node WHERE status = 'pendingdestroy'`;
    db.query(sql, (err, result) => {
        if (err) {
            res.status(200).json({ error: "Error listing node" });
        } else {
            if (result.length === 0) {
                res.status(200).json({ message: "No node found" });
                return;
            } else {
                res.status(200).json({ nodes: result });
            }
        }
    });
});

app.get("*", (req, res) => {
    res.status(404).json({ error: "Route not found" });
});

module.exports = app;